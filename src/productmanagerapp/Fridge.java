/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productmanagerapp;

/**
 *
 * @author medel
 */
public class Fridge extends Product {

    private int numDoors;

    //constructors
    public Fridge(String code, String name, double price, int numDoors) {
        super(code, name, price);
        this.numDoors = numDoors;
    }

    public Fridge(String code) {
        super(code);
    }

    public Fridge(Fridge other) {
        super(other.code, other.name, other.price);
        this.numDoors = other.numDoors;
    }

    //getters and setters
    public int getNumDoors() {
        return numDoors;
    }

    public void setNumDoors(int numDoors) {
        this.numDoors = numDoors;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\"Fridge\":{");
        sb.append("\"doors\"=");
        sb.append(numDoors);
        sb.append("; ");
        sb.append(super.toString());
        sb.append("}");
        return sb.toString();
    }

}
